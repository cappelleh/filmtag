package be.hcpl.android.filmtag

import android.os.Bundle
import android.preference.PreferenceManager
import android.text.TextUtils
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.widget.ArrayAdapter
import android.widget.AutoCompleteTextView
import android.widget.CheckBox
import android.widget.EditText
import android.widget.Toast
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import be.hcpl.android.filmtag.FilmFrameListFragment.Companion.KEY_FILM_ROLL

import java.util.Arrays

import be.hcpl.android.filmtag.model.Roll
import be.hcpl.android.filmtag.util.StorageUtil

/**
 * Created by hcpl on 1/08/15.
 */
class EditRollFragment : Fragment(R.layout.fragment_form_roll) {

    private var roll: Roll? = null

    private lateinit var edit_type: AutoCompleteTextView
    private lateinit var edit_notes: EditText
    private lateinit var edit_exposed: EditText
    private lateinit var edit_frames: EditText
    private lateinit var check_developed: CheckBox
    private lateinit var edit_tags: EditText

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putSerializable(KEY_FILM_ROLL, roll)
    }

    override fun onViewStateRestored(savedInstanceState: Bundle?) {
        super.onViewStateRestored(savedInstanceState)
        if (savedInstanceState != null)
            roll = savedInstanceState.getSerializable(KEY_FILM_ROLL) as Roll?
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
        val args = arguments
        if (args != null) {
            roll = args.getSerializable(KEY_FILM_ROLL) as Roll?
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.new_film, menu)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        edit_type = view.findViewById(R.id.edit_type)
        edit_notes = view.findViewById(R.id.edit_notes)
        edit_exposed = view.findViewById(R.id.edit_exposed)
        edit_frames = view.findViewById(R.id.edit_frames)
        check_developed = view.findViewById(R.id.check_developed)
        edit_tags = view.findViewById(R.id.edit_tags)

        // prefill data if possible
        if (roll != null) {
            edit_type.setText(roll!!.type)
            edit_notes.setText(roll!!.notes)
            if (roll!!.speed != 0)
                edit_exposed.setText(roll!!.speed.toString())
            if (roll!!.frames != 0)
                edit_frames.setText(roll!!.frames.toString())
            check_developed.isChecked = roll!!.isDeveloped
            // populate the tags here
            if (!roll!!.tags.isEmpty())
                edit_tags.setText(TextUtils.join(" ", roll!!.tags))
        } else {
            // have preferences for this
            val prefs = PreferenceManager.getDefaultSharedPreferences(context)
            edit_exposed.setText(prefs.getString("key_default_iso", 200.toString()))
            edit_frames.setText(prefs.getString("key_default_frames", 36.toString()))
        }// populate with defaults here

        // autocomplete
        val adapter = ArrayAdapter(requireContext(),
                android.R.layout.simple_dropdown_item_1line, typeSuggestions)
        edit_type.setAdapter(adapter)
    }

    private val typeSuggestions: Array<String>
        get() {
            val rolls = StorageUtil.getAllRolls(activity as MainActivity)
            val existingTypes = arrayOfNulls<String>(rolls.size)
            for (i in rolls.indices) {
                existingTypes[i] = rolls[i].type
            }
            return existingTypes as Array<String>
        }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_create -> {
                createNewItem()
                return true
            }
            android.R.id.home -> {
                toOverviewOrDetail()
                return true
            }
        }
        return false
    }

    private fun toOverviewOrDetail() {
        if (roll == null) {
            findNavController().navigate(R.id.action_home)
        } else {
            findNavController().navigate(R.id.action_detail, bundleOf(KEY_FILM_ROLL to roll))
        }
    }

    private fun createNewItem() {
        var newRoll = false
        // insert the new item
        if (roll == null) {
            roll = Roll()
            newRoll = true
        }
        roll!!.type = edit_type.text.toString()
        roll!!.notes = edit_notes.text.toString()
        roll!!.isDeveloped = check_developed.isChecked
        roll!!.tags = Arrays.asList(*TextUtils.split(edit_tags.text.toString(), " "))
        try {
            roll!!.speed = Integer.parseInt(edit_exposed.text.toString())
        } catch (nfe: NumberFormatException) {
            Toast.makeText(activity, R.string.err_parsing_failed, Toast.LENGTH_SHORT).show()
        }

        try {
            roll!!.frames = Integer.parseInt(edit_frames.text.toString())
        } catch (nfe: NumberFormatException) {
            Toast.makeText(activity, R.string.err_parsing_failed, Toast.LENGTH_SHORT).show()
        }

        // store new roll
        if (newRoll)
            StorageUtil.addNewRoll(activity as MainActivity, roll!!)
        else
            StorageUtil.updateRoll(activity as MainActivity, roll!!)

        // navigate to overview
        toOverviewOrDetail()
    }

}
